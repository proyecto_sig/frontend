import { Component, OnInit } from '@angular/core';
import { MapService } from 'src/app/service/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor(private service : MapService) { }

  catast: any;

  
  ngOnInit() {
    this.service.msg = "Cargando . . ."
    this.service.getVB().subscribe(
      reponse => {
        var res = reponse['result'][0]
        document.getElementById("vb").setAttribute("viewBox",res.xmin + ' ' + res.ymax + ' ' + res.ancho + ' ' + res.alto);
      }
    );
    this.service.getFigures();
  }

  look(cat){
    this.catast = cat;
  }

}

