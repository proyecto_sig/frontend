import { Component, OnInit } from '@angular/core';
import { MapService } from 'src/app/service/map.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(private service:MapService) { }

  msgRef = ' ';
  msgAdm = ' ';
  groupInfo: any;
  codes = [];


  ngOnInit() {
  }
  
  clear(){
    this.msgRef = ' ';
  }

  onRegister(){
    if (this.service.formRegister.valid){
      this.service.onRegister().subscribe(response =>{
        if(response["Success"]==false){
          this.msgRef = "Ya este nombre de usuario existe"
        }else{
          this.msgRef = "Inicio satisfactorio"
        }
      });
    }else{
      this.msgRef = "Campo obligatorio"
    }
  }

  iniciar(){
    if (this.service.formAdm.valid){
      this.service.verifyUser().subscribe(response =>{
        if(response["Success"]==false){
          this.msgAdm = "Hashtag o contraseña invalido"
          this.groupInfo = null;
        }else{
          this.msgAdm = "Inicio exitoso"
          this.groupInfo = response['Grupo'];
          this.getGroupCodes();
        }
      });
    }else{
      this.msgAdm = "Campos obligatorios"
    }
  }

  onCreateGroup(){
    if (this.service.formGroup.valid){
      this.service.onCreateGroup().subscribe(response =>{
        if(response["Success"]==false){
          this.msgRef = "Ya existe un grupo con este hashtag"
        }else{
          this.msgRef = "Grupo registrado con éxito"
        }
      });
    }else{
      this.msgRef = "Campos obligatorios"
    }
  }

  getGroupCodes(){
    this.service.groupCodes(this.groupInfo.g_idgrupo).subscribe(response =>{
      this.codes = response['result']
    })
  }

  deleteCode(code){
    this.service.deleteCode(code.g_idcodigo).subscribe(response => {
      this.getGroupCodes()
    });
  }

  onCreateCode(){
    if (this.service.formCode.valid){
      this.service.createCode(this.groupInfo.g_idgrupo).subscribe(response =>{
        if(response["Success"]==false){
          this.msgRef = "Ha ocurrido un error"
        }else{
          this.msgRef = "Código creado con exito"
          this.getGroupCodes();
        }
      });
    }else{
      this.msgRef = "Campos obligatorios"
    }
  }

}
