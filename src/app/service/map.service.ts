import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient) { }


  formRegister: FormGroup = new FormGroup({
    username: new FormControl('',Validators.required),
  });

  formGroup: FormGroup = new FormGroup({
    name: new FormControl('',Validators.required),
    code: new FormControl('',Validators.required),
    fun: new FormControl('',Validators.required),
    hash: new FormControl('',Validators.required)
  });

  formAdm: FormGroup = new FormGroup({
    user: new FormControl('',Validators.required),
    code: new FormControl('',Validators.required),
    hash: new FormControl('',Validators.required)
  });

  formCode: FormGroup = new FormGroup({
    role: new FormControl('',Validators.required),
    code: new FormControl('',Validators.required)
  });

  svgs = [];
  vb : any;
  msg = ' ';
  uri = "http://localhost:7500/prueba"

  onRegister(){
    const obj = {
      nombre: this.formRegister.controls["username"].value
    };
    return this.http.post(this.uri+'/insertarUsuario', obj)
  }

  onCreateGroup(){
    const obj = {
      nombreGrupo: this.formGroup.controls["name"].value,
      tag: this.formGroup.controls["hash"].value,
      codigo: this.formGroup.controls["code"].value,
      funcionalidad: this.formGroup.controls["fun"].value,
    };
    return this.http.post(this.uri+'/insertarGrupo', obj)
  }

  createCode(id){
    const obj = {
      idGrupo: id,
      nombre: this.formCode.controls["code"].value,
      rol: this.formCode.controls["role"].value
    };
    return this.http.post(this.uri+'/insertarCodigo', obj)
  }

  getFigures() {
    this.http.get(this.uri+'/getFigureSVG').subscribe(
      reponse => {
        this.svgs = reponse['result']
        this.msg = " "

      }
    )
  }

  getVB() {
    return this.http.get(this.uri+'/getBoundingBox')
  }

  verifyUser(){
    const obj = {
      tag: this.formAdm.controls["hash"].value,
      codigo: this.formAdm.controls["code"].value,
      usuario: this.formAdm.controls["user"].value,
    };
    return this.http.post(this.uri+'/verificarCodigo', obj)
  }

  groupCodes(id){
    return this.http.get(this.uri+'/getCodigoGrupo', {
      params : new HttpParams()
      .set("idGrupo" , id)
    })
  }

  deleteCode(code){
    const obj = {
      idCodigo: code
    };
    return this.http.post(this.uri+'/deleteCodigo', obj)
  }


}
